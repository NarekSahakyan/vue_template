// Main Importes
import Vue from 'vue'
import Router from 'vue-router'
import VueAxios from 'vue-axios'
import axios from 'axios';
import jQuery from 'jquery';
import Interceptor from './services/Interceptor'
import BootstrapVue from 'bootstrap-vue'
import bNavbar from 'bootstrap-vue/es/components/navbar/navbar';
import apiPath from './configApi'

// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// CSS
require('bootstrap/dist/css/bootstrap.min.css');
require('./assets/css/loading.css');
require('./assets/css/loading-btn.css');
require('./assets/css/default.css');
require('./assets/css/buttons.css');
require('./assets/css/fonts.css');
require('./assets/css/style.css');
// JS
global.$ = jQuery;

// Components
import App from './App.vue'
import Home from './components/Home.vue'
import NotFound from './components/404.vue'

Vue.component('b-navbar', bNavbar);

Vue.config.productionTip = false;

axios.defaults.baseURL = apiPath.apiPath;

// Vue.use(NavBar);
Vue.use(Router);
Vue.use(VueAxios, axios);
Vue.use(Interceptor);
Vue.use(BootstrapVue);

const router = new Router({
	mode: 'history',
	routes: [
		{path: '*', component: NotFound, name: '404'},
		{path: '/', component: Home, name: 'home', meta: {requiresVisitors: true}},
	]
});
/* eslint-disable */
router.beforeEach((to, from, next) => {
	// if (to.meta.requiresAuth1) {
	// 	const authUser = JSON.parse(window.localStorage.getItem('authUser'));
	// 	console.log(authUser)
	// 	if (authUser && authUser.access_token) {
	// 		next()
	// 	} else {
	// 		next({name: 'login'})
	// 	}
	// } else if (to.meta.requiresVisitors) {
	// 	const authUser = JSON.parse(window.localStorage.getItem('authUser'));
	// 	if (authUser && authUser.access_token) {
	// 		next({name: 'company'})
	// 	} else {
	// 		next()
	// 	}
	// }
	next()
});

new Vue({
	router,
	mode: 'history',
  render: h => h(App)
}).$mount('#app');


